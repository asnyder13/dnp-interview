import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';

import { BookModel } from '../interfaces/book.interface';

@Injectable({
	providedIn: 'root',
})
export class BookService {
	private endpoint = 'https://booksapi.azurewebsites.net/api/';

	constructor(private http: HttpClient) {}

	/**
	 * Fetch all books.
	 * @returns All currnet books.
	 */
	public getAllBooks(): Observable<BookModel[]> {
		return this.http.get<BookModel[]>(this.endpoint + 'Books');
	}

	/**
	 * Delete a book and return list of remaining books.
	 * @param bookID ID of book to be deleted.
	 * @returns List current books.
	 */
	public deleteBook(bookID: string | number) {
		return this.http.delete<BookModel[]>(this.endpoint + 'Books/' + bookID);
	}
}
