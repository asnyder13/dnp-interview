import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { GridModule } from '@progress/kendo-angular-grid';
import { HttpClientModule } from '@angular/common/http';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';

import { AppComponent } from './app.component';
import { BookService } from './services/book-service.service';
import { BookDescriptionComponent } from './components/book-description.component';

@NgModule({
	declarations: [AppComponent, BookDescriptionComponent],
	imports: [
		BrowserAnimationsModule,
		BrowserModule,
		FormsModule,
		GridModule,
		HttpClientModule,
		MatButtonModule,
		MatDialogModule,
		MatIconModule,
		MatInputModule,
		MatSelectModule,
	],
	providers: [BookService],
	bootstrap: [AppComponent],
})
export class AppModule {}
