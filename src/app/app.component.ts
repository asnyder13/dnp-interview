import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { SortDescriptor } from '@progress/kendo-data-query';

import { BookDescriptionComponent } from './components/book-description.component';
import { BookModel } from './interfaces/book.interface';
import { BookService } from './services/book-service.service';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
	/**
	 * Depending on the functionality of the site, I would consider keeping "allBooks" as an observable
	 * and populating the grid from a secondary Observable that re-emits based on debounced filter changes.
	 * However this sort of change would need to be carefully considered, as over-complicated observables
	 * and be pretty rough for readability/maintainability.
	 */
	public allBooks: BookModel[] = [];
	public filteredBooks: BookModel[] = [];
	// Separate the sorted books, in order to allow a return to the "unsorted" state.
	public sortedBooks: BookModel[] = [];
	public title = 'Books!';
	public sort: SortDescriptor[] = [];
	public columns = [
		'Author',
		'Title',
		'Genre',
		'Price',
		'Publish Date',
		'Description',
		'Delete',
	];
	public visibleColumns: string[] = this.columns;

	constructor(private bookService: BookService, private dialog: MatDialog) {}

	async ngOnInit() {
		const bookList = await this.bookService.getAllBooks().toPromise();
		this.resetBookLists(bookList);
	}

	/**
	 * Filter the full list of books, and ensure sorting.
	 * @param filterValue String value of the filter input.
	 */
	public filterChange(filterValue?: string): void {
		if (filterValue !== undefined) {
			const filterValueLower = filterValue.toLowerCase();
			this.filteredBooks = this.allBooks.filter((book) => {
				const combProp = Object.entries(book)
					.filter(([k, v]) => k !== 'description' && v !== undefined)
					.map(([_, v]) => v?.toString())
					.join(' ')
					.toLowerCase();
				return combProp.includes(filterValueLower);
			});
		} else {
			this.filteredBooks = this.allBooks;
		}

		// If filter changes, maintain the existing sort.
		this.gridSortChange(this.sort);
	}

	/**
	 * If a sort is present, then reorder the filtered list.
	 * @param sort KendoUI SortDescriptor.
	 */
	public gridSortChange(sort: SortDescriptor[]): void {
		// Currently the grid only allows for sorting of a single column at a time.
		if (sort[0]) {
			this.sort = sort;
			this.sortedBooks = this.filteredBooks.sort((a, b) => {
				let order = this.compare(a[sort[0].field], b[sort[0].field]);
				if (sort[0].dir === 'desc' && order !== 0) {
					order = order * -1;
				}

				return order;
			});
		} else {
			this.sortedBooks = this.filteredBooks;
		}
	}

	/**
	 * Trigger the Material Dialog to display the book's description.
	 * @param book
	 */
	public displayDescription(book: BookModel): void {
		this.dialog.open(BookDescriptionComponent, { data: book, width: '50%' });
	}

	/**
	 * Delete a book from the list.
	 * @param book
	 */
	public async deleteBook(book: BookModel): Promise<void> {
		if (book.id) {
			const newBookList = await this.bookService
				.deleteBook(book.id)
				.toPromise();
			this.resetBookLists(newBookList);
		}
	}

	/**
	 * Reset the book lists.
	 * @param bookList
	 */
	private resetBookLists(bookList: BookModel[]): void {
		this.allBooks = bookList;
		this.filteredBooks = bookList;
		this.sortedBooks = bookList;
		this.gridSortChange(this.sort);
	}

	/**
	 * General comparison function.
	 * @param a First member.
	 * @param b Second member.
	 * @returns Sort order.
	 */
	private compare<T>(a: T, b: T): number {
		if (a < b) return -1;
		else if (a > b) return 1;
		else return 0;
	}
}
