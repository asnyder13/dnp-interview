export interface BookModel {
	[key: string]: string | number | undefined;
	id?: string;
	author?: string;
	title?: string;
	genre?: string;
	price?: string | number;
	publishDate?: string;
	description?: string;
}
