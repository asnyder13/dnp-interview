import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { BookModel } from '../interfaces/book.interface';

@Component({
	selector: 'app-book-description',
	template: `
		<h2 mat-dialog-title>{{ data.title }} Description</h2>
		<div mat-dialog-content>
			{{ data.description }}
		</div>
		<div mat-dialog-actions>
			<button mat-button (click)="closeClick()">Ok</button>
		</div>
	`,
	styles: [],
})
export class BookDescriptionComponent implements OnInit {
	constructor(
		public dialogRef: MatDialogRef<BookDescriptionComponent>,
		@Inject(MAT_DIALOG_DATA) public data: BookModel
	) {}

	ngOnInit(): void {}

	public closeClick(): void {
		this.dialogRef.close();
	}
}
